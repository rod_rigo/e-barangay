<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CivilStatusesFixture
 */
class CivilStatusesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'civil_status' => 'Lorem ipsum dolor sit amet',
                'created' => 1716862319,
                'modified' => 1716862319,
            ],
        ];
        parent::init();
    }
}
