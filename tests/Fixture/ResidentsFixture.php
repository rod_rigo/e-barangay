<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ResidentsFixture
 */
class ResidentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'resident_image' => 'Lorem ipsum dolor sit amet',
                'contact_number' => 'Lorem ipsum dolor sit amet',
                'purok_id' => 1,
                'civil_status_id' => 1,
                'gender_id' => 1,
                'birth_date' => '2024-05-28',
                'age' => 1,
                'residential_id' => 1,
                'created' => 1716865749,
                'modified' => 1716865749,
            ],
        ];
        parent::init();
    }
}
