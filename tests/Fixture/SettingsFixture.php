<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SettingsFixture
 */
class SettingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'captain' => 'Lorem ipsum dolor sit amet',
                'or_number' => 'Lorem ipsum dolor sit amet',
                'ctc_number' => 'Lorem ipsum dolor sit amet',
                'community_tax_number' => 'Lorem ipsum dolor sit amet',
                'issued_at' => 'Lorem ipsum dolor sit amet',
                'created' => 1717339985,
                'modified' => 1717339985,
            ],
        ];
        parent::init();
    }
}
