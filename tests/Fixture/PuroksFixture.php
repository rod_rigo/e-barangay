<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PuroksFixture
 */
class PuroksFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'purok' => 'Lorem ipsum dolor sit amet',
                'created' => 1716862759,
                'modified' => 1716862759,
            ],
        ];
        parent::init();
    }
}
