<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BusinessClearancesFixture
 */
class BusinessClearancesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'resident_id' => 1,
                'business_name' => 'Lorem ipsum dolor sit amet',
                'business_nature' => 'Lorem ipsum dolor sit amet',
                'community_tax_no' => 'Lorem ipsum dolor sit amet',
                'issued_date' => '2024-06-03 02:35:14',
                'amount_paid' => 1,
                'or_no' => 'Lorem ipsum dolor sit amet',
                'validity' => '2024-06-03',
                'created' => 1717382114,
                'modified' => 1717382114,
            ],
        ];
        parent::init();
    }
}
