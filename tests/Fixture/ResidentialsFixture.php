<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ResidentialsFixture
 */
class ResidentialsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'residential' => 'Lorem ipsum dolor sit amet',
                'created' => 1716863065,
                'modified' => 1716863065,
            ],
        ];
        parent::init();
    }
}
