<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GendersFixture
 */
class GendersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'gender' => 'Lorem ipsum dolor sit amet',
                'created' => 1716861181,
                'modified' => 1716861181,
            ],
        ];
        parent::init();
    }
}
