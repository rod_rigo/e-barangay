<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResidentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResidentsTable Test Case
 */
class ResidentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ResidentsTable
     */
    protected $Residents;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Residents',
        'app.Puroks',
        'app.CivilStatuses',
        'app.Genders',
        'app.Residentials',
        'app.Issuances',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Residents') ? [] : ['className' => ResidentsTable::class];
        $this->Residents = $this->getTableLocator()->get('Residents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Residents);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ResidentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ResidentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
