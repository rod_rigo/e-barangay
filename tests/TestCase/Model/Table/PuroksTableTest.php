<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PuroksTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PuroksTable Test Case
 */
class PuroksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PuroksTable
     */
    protected $Puroks;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Puroks',
        'app.Residents',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Puroks') ? [] : ['className' => PuroksTable::class];
        $this->Puroks = $this->getTableLocator()->get('Puroks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Puroks);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PuroksTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
