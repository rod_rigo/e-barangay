<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResidentialsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResidentialsTable Test Case
 */
class ResidentialsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ResidentialsTable
     */
    protected $Residentials;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Residentials',
        'app.Residents',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Residentials') ? [] : ['className' => ResidentialsTable::class];
        $this->Residentials = $this->getTableLocator()->get('Residentials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Residentials);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ResidentialsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
