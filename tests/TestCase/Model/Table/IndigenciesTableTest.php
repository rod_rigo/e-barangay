<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IndigenciesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IndigenciesTable Test Case
 */
class IndigenciesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\IndigenciesTable
     */
    protected $Indigencies;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Indigencies',
        'app.Residents',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Indigencies') ? [] : ['className' => IndigenciesTable::class];
        $this->Indigencies = $this->getTableLocator()->get('Indigencies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Indigencies);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\IndigenciesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\IndigenciesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
