<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clearances Model
 *
 * @property \App\Model\Table\ResidentsTable&\Cake\ORM\Association\BelongsTo $Residents
 *
 * @method \App\Model\Entity\Clearance newEmptyEntity()
 * @method \App\Model\Entity\Clearance newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Clearance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Clearance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Clearance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Clearance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Clearance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Clearance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clearance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clearance[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clearance[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clearance[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clearance[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClearancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('clearances');
        $this->setDisplayField('clearance_no');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Residents', [
            'foreignKey' => 'resident_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('resident_id')
            ->notEmptyString('resident_id');

        $validator
            ->scalar('clearance_no')
            ->maxLength('clearance_no', 255)
            ->requirePresence('clearance_no', 'create')
            ->notEmptyString('clearance_no');

        $validator
            ->scalar('purpose')
            ->requirePresence('purpose', 'create')
            ->notEmptyString('purpose');

        $validator
            ->date('issued_date')
            ->requirePresence('issued_date', 'create')
            ->notEmptyDate('issued_date');

        $validator
            ->numeric('amount_paid')
            ->requirePresence('amount_paid', 'create')
            ->notEmptyString('amount_paid');

        $validator
            ->date('validity')
            ->requirePresence('validity', 'create')
            ->notEmptyDate('validity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('resident_id', 'Residents'), ['errorField' => 'resident_id']);

        return $rules;
    }
}
