<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Residents Model
 *
 * @property \App\Model\Table\PuroksTable&\Cake\ORM\Association\BelongsTo $Puroks
 * @property \App\Model\Table\CivilStatusesTable&\Cake\ORM\Association\BelongsTo $CivilStatuses
 * @property \App\Model\Table\GendersTable&\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\ResidentialsTable&\Cake\ORM\Association\BelongsTo $Residentials
 * @property \App\Model\Table\IssuancesTable&\Cake\ORM\Association\HasMany $Issuances
 *
 * @method \App\Model\Entity\Resident newEmptyEntity()
 * @method \App\Model\Entity\Resident newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Resident[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Resident get($primaryKey, $options = [])
 * @method \App\Model\Entity\Resident findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Resident patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Resident[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Resident|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Resident saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResidentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('residents');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Puroks', [
            'foreignKey' => 'purok_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CivilStatuses', [
            'foreignKey' => 'civil_status_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Residentials', [
            'foreignKey' => 'residential_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Issuances', [
            'foreignKey' => 'resident_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('contact_number')
            ->maxLength('contact_number', 255)
            ->allowEmptyString('contact_number');

        $validator
            ->integer('purok_id')
            ->notEmptyString('purok_id');

        $validator
            ->integer('civil_status_id')
            ->notEmptyString('civil_status_id');

        $validator
            ->integer('gender_id')
            ->notEmptyString('gender_id');

        $validator
            ->date('birth_date')
            ->requirePresence('birth_date', 'create')
            ->notEmptyDate('birth_date');

        $validator
            ->integer('age')
            ->requirePresence('age', 'create')
            ->notEmptyString('age');

        $validator
            ->integer('residential_id')
            ->notEmptyString('residential_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('purok_id', 'Puroks'), ['errorField' => 'purok_id']);
        $rules->add($rules->existsIn('civil_status_id', 'CivilStatuses'), ['errorField' => 'civil_status_id']);
        $rules->add($rules->existsIn('gender_id', 'Genders'), ['errorField' => 'gender_id']);
        $rules->add($rules->existsIn('residential_id', 'Residentials'), ['errorField' => 'residential_id']);

        return $rules;
    }
}
