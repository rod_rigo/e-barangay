<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Puroks Model
 *
 * @property \App\Model\Table\ResidentsTable&\Cake\ORM\Association\HasMany $Residents
 *
 * @method \App\Model\Entity\Purok newEmptyEntity()
 * @method \App\Model\Entity\Purok newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Purok[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Purok get($primaryKey, $options = [])
 * @method \App\Model\Entity\Purok findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Purok patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Purok[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Purok|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purok saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purok[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Purok[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Purok[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Purok[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PuroksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('puroks');
        $this->setDisplayField('purok');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Residents', [
            'foreignKey' => 'purok_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('purok')
            ->maxLength('purok', 255)
            ->requirePresence('purok', 'create')
            ->notEmptyString('purok');

        return $validator;
    }
}
