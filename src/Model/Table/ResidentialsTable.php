<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Residentials Model
 *
 * @property \App\Model\Table\ResidentsTable&\Cake\ORM\Association\HasMany $Residents
 *
 * @method \App\Model\Entity\Residential newEmptyEntity()
 * @method \App\Model\Entity\Residential newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Residential[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Residential get($primaryKey, $options = [])
 * @method \App\Model\Entity\Residential findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Residential patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Residential[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Residential|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Residential saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Residential[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Residential[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Residential[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Residential[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResidentialsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('residentials');
        $this->setDisplayField('residential');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Residents', [
            'foreignKey' => 'residential_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('residential')
            ->maxLength('residential', 255)
            ->requirePresence('residential', 'create')
            ->notEmptyString('residential');

        return $validator;
    }
}
