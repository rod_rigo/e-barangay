<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BusinessClearances Model
 *
 * @property \App\Model\Table\ResidentsTable&\Cake\ORM\Association\BelongsTo $Residents
 *
 * @method \App\Model\Entity\BusinessClearance newEmptyEntity()
 * @method \App\Model\Entity\BusinessClearance newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\BusinessClearance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BusinessClearance get($primaryKey, $options = [])
 * @method \App\Model\Entity\BusinessClearance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\BusinessClearance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessClearance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessClearance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessClearance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessClearance[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\BusinessClearance[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\BusinessClearance[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\BusinessClearance[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BusinessClearancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('business_clearances');
        $this->setDisplayField('business_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Residents', [
            'foreignKey' => 'resident_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('resident_id')
            ->notEmptyString('resident_id');

        $validator
            ->scalar('business_name')
            ->maxLength('business_name', 255)
            ->requirePresence('business_name', 'create')
            ->notEmptyString('business_name');

        $validator
            ->scalar('business_nature')
            ->maxLength('business_nature', 255)
            ->requirePresence('business_nature', 'create')
            ->notEmptyString('business_nature');

        $validator
            ->scalar('community_tax_no')
            ->maxLength('community_tax_no', 255)
            ->requirePresence('community_tax_no', 'create')
            ->notEmptyString('community_tax_no');

        $validator
            ->dateTime('issued_date')
            ->requirePresence('issued_date', 'create')
            ->notEmptyDateTime('issued_date');

        $validator
            ->numeric('amount_paid')
            ->requirePresence('amount_paid', 'create')
            ->notEmptyString('amount_paid');

        $validator
            ->scalar('or_no')
            ->maxLength('or_no', 255)
            ->requirePresence('or_no', 'create')
            ->notEmptyString('or_no');

        $validator
            ->date('validity')
            ->requirePresence('validity', 'create')
            ->notEmptyDate('validity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('resident_id', 'Residents'), ['errorField' => 'resident_id']);

        return $rules;
    }
}
