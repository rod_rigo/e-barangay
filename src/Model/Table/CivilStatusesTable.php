<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CivilStatuses Model
 *
 * @property \App\Model\Table\ResidentsTable&\Cake\ORM\Association\HasMany $Residents
 *
 * @method \App\Model\Entity\CivilStatus newEmptyEntity()
 * @method \App\Model\Entity\CivilStatus newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\CivilStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CivilStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\CivilStatus findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\CivilStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CivilStatus[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\CivilStatus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CivilStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CivilStatusesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('civil_statuses');
        $this->setDisplayField('civil_status');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Residents', [
            'foreignKey' => 'civil_status_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('civil_status')
            ->maxLength('civil_status', 255)
            ->requirePresence('civil_status', 'create')
            ->notEmptyString('civil_status');

        return $validator;
    }
}
