<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Resident Entity
 *
 * @property int $id
 * @property string $name
 * @property string $resident_image
 * @property string|null $contact_number
 * @property int $purok_id
 * @property int $civil_status_id
 * @property int $gender_id
 * @property \Cake\I18n\FrozenDate $birth_date
 * @property int $age
 * @property int $residential_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Purok $purok
 * @property \App\Model\Entity\CivilStatus $civil_status
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\Residential $residential
 * @property \App\Model\Entity\Issuance[] $issuances
 */
class Resident extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'resident_image' => true,
        'contact_number' => true,
        'purok_id' => true,
        'civil_status_id' => true,
        'gender_id' => true,
        'birth_date' => true,
        'age' => true,
        'residential_id' => true,
        'created' => true,
        'modified' => true,
        'purok' => true,
        'civil_status' => true,
        'gender' => true,
        'residential' => true,
        'issuances' => true,
    ];

}
