<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BusinessClearance Entity
 *
 * @property int $id
 * @property int $resident_id
 * @property string $business_name
 * @property string $business_nature
 * @property string $community_tax_no
 * @property \Cake\I18n\FrozenTime $issued_date
 * @property float $amount_paid
 * @property string $or_no
 * @property \Cake\I18n\FrozenDate $validity
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Resident $resident
 */
class BusinessClearance extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'resident_id' => true,
        'business_name' => true,
        'business_nature' => true,
        'community_tax_no' => true,
        'issued_date' => true,
        'amount_paid' => true,
        'or_no' => true,
        'validity' => true,
        'created' => true,
        'modified' => true,
        'resident' => true,
    ];
}
