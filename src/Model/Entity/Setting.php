<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Setting Entity
 *
 * @property int $id
 * @property string $captain
 * @property string $or_number
 * @property string $ctc_number
 * @property string $community_tax_number
 * @property string $issued_at
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Setting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'captain' => true,
        'or_number' => true,
        'ctc_number' => true,
        'community_tax_number' => true,
        'issued_at' => true,
        'created' => true,
        'modified' => true,
    ];
}
