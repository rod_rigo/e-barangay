<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Indigency Entity
 *
 * @property int $id
 * @property int $resident_id
 * @property int $clearance_no
 * @property string $purpose
 * @property \Cake\I18n\FrozenDate $issued_date
 * @property float $amount_paid
 * @property \Cake\I18n\FrozenDate $validity
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Resident $resident
 */
class Indigency extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'resident_id' => true,
        'clearance_no' => true,
        'purpose' => true,
        'issued_date' => true,
        'amount_paid' => true,
        'validity' => true,
        'created' => true,
        'modified' => true,
        'resident' => true,
    ];
}
