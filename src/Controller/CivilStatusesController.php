<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * CivilStatuses Controller
 *
 * @property \App\Model\Table\CivilStatusesTable $CivilStatuses
 * @method \App\Model\Entity\CivilStatus[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CivilStatusesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $civilStatuses = $this->paginate($this->CivilStatuses);

        $this->set(compact('civilStatuses'));
    }

    /**
     * View method
     *
     * @param string|null $id Civil Status id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $civilStatus = $this->CivilStatuses->get($id, [
            'contain' => ['Residents'],
        ]);

        $this->set(compact('civilStatus'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $civilStatus = $this->CivilStatuses->newEmptyEntity();
        if ($this->request->is('post')) {
            $civilStatus = $this->CivilStatuses->patchEntity($civilStatus, $this->request->getData());
            if ($this->CivilStatuses->save($civilStatus)) {
                $this->Flash->success(__('The civil status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The civil status could not be saved. Please, try again.'));
        }
        $this->set(compact('civilStatus'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Civil Status id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $civilStatus = $this->CivilStatuses->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $civilStatus = $this->CivilStatuses->patchEntity($civilStatus, $this->request->getData());
            if ($this->CivilStatuses->save($civilStatus)) {
                $this->Flash->success(__('The civil status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The civil status could not be saved. Please, try again.'));
        }
        $this->set(compact('civilStatus'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Civil Status id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $civilStatus = $this->CivilStatuses->get($id);
        if ($this->CivilStatuses->delete($civilStatus)) {
            $this->Flash->success(__('The civil status has been deleted.'));
        } else {
            $this->Flash->error(__('The civil status could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
