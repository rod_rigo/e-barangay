<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * BusinessClearances Controller
 *
 * @property \App\Model\Table\BusinessClearancesTable $BusinessClearances
 * @method \App\Model\Entity\BusinessClearance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BusinessClearancesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Residents'],
        ];
        $businessClearances = $this->paginate($this->BusinessClearances);

        $this->set(compact('businessClearances'));
    }

    /**
     * View method
     *
     * @param string|null $id Business Clearance id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $businessClearance = $this->BusinessClearances->get($id, [
            'contain' => ['Residents.Puroks'],
        ]);
        $setting = TableRegistry::getTableLocator()->get('Settings')->find()
            ->firstOrFail();

        $this->set(compact('businessClearance', 'setting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $businessClearance = $this->BusinessClearances->newEmptyEntity();
        $setting = TableRegistry::getTableLocator()->get('Settings')->find()
            ->firstOrFail();
        if ($this->request->is('post')) {
            $businessClearance = $this->BusinessClearances->patchEntity($businessClearance, $this->request->getData());
            if ($this->BusinessClearances->save($businessClearance)) {
                $this->Flash->success(__('The business clearance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The business clearance could not be saved. Please, try again.'));
        }
        $residents = $this->BusinessClearances->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
            ->all();
        $this->set(compact('businessClearance', 'residents', 'setting'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Business Clearance id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $businessClearance = $this->BusinessClearances->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $businessClearance = $this->BusinessClearances->patchEntity($businessClearance, $this->request->getData());
            if ($this->BusinessClearances->save($businessClearance)) {
                $this->Flash->success(__('The business clearance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The business clearance could not be saved. Please, try again.'));
        }
        $residents = $this->BusinessClearances->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
            ->all();
        $this->set(compact('businessClearance', 'residents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Business Clearance id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $businessClearance = $this->BusinessClearances->get($id);
        if ($this->BusinessClearances->delete($businessClearance)) {
            $this->Flash->success(__('The business clearance has been deleted.'));
        } else {
            $this->Flash->error(__('The business clearance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
