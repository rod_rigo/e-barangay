<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * Clearances Controller
 *
 * @property \App\Model\Table\ClearancesTable $Clearances
 * @method \App\Model\Entity\Clearance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClearancesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Residents'],
        ];
        $clearances = $this->paginate($this->Clearances);

        $this->set(compact('clearances'));
    }

    /**
     * View method
     *
     * @param string|null $id Clearance id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $clearance = $this->Clearances->get($id, [
            'contain' => ['Residents.Puroks'],
        ]);
        $setting = TableRegistry::getTableLocator()->get('Settings')->find()
            ->firstOrFail();

        $this->set(compact('clearance', 'setting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clearance = $this->Clearances->newEmptyEntity();
        $max = $this->Clearances->find()->max('clearance_no')->clearance_no;
        if ($this->request->is('post')) {
            $clearance = $this->Clearances->patchEntity($clearance, $this->request->getData());
            $clearance->clearance_no = (intval($max) + intval(1));
            if ($this->Clearances->save($clearance)) {
                $this->Flash->success(__('The clearance has been saved.'));

                return $this->redirect(['action' => 'view', intval($clearance->id)]);
            }
            $this->Flash->error(__('The clearance could not be saved. Please, try again.'));
        }
        $residents = $this->Clearances->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
        ->all();
        $this->set(compact('clearance', 'residents', 'max'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Clearance id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clearance = $this->Clearances->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clearance = $this->Clearances->patchEntity($clearance, $this->request->getData());
            if ($this->Clearances->save($clearance)) {
                $this->Flash->success(__('The clearance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The clearance could not be saved. Please, try again.'));
        }
        $residents = $this->Clearances->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
            ->all();
        $this->set(compact('clearance', 'residents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Clearance id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clearance = $this->Clearances->get($id);
        if ($this->Clearances->delete($clearance)) {
            $this->Flash->success(__('The clearance has been deleted.'));
        } else {
            $this->Flash->error(__('The clearance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
