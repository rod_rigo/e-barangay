<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Puroks Controller
 *
 * @property \App\Model\Table\PuroksTable $Puroks
 * @method \App\Model\Entity\Purok[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PuroksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $puroks = $this->paginate($this->Puroks);

        $this->set(compact('puroks'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $purok = $this->Puroks->newEmptyEntity();
        if ($this->request->is('post')) {
            $purok = $this->Puroks->patchEntity($purok, $this->request->getData());
            if ($this->Puroks->save($purok)) {
                $this->Flash->success(__('The purok has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The purok could not be saved. Please, try again.'));
        }
        $this->set(compact('purok'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Purok id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $purok = $this->Puroks->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $purok = $this->Puroks->patchEntity($purok, $this->request->getData());
            if ($this->Puroks->save($purok)) {
                $this->Flash->success(__('The purok has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The purok could not be saved. Please, try again.'));
        }
        $this->set(compact('purok'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Purok id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purok = $this->Puroks->get($id);
        if ($this->Puroks->delete($purok)) {
            $this->Flash->success(__('The purok has been deleted.'));
        } else {
            $this->Flash->error(__('The purok could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
