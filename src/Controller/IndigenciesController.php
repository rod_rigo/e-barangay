<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * Indigencies Controller
 *
 * @property \App\Model\Table\IndigenciesTable $Indigencies
 * @method \App\Model\Entity\Indigency[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IndigenciesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Residents'],
        ];
        $indigencies = $this->paginate($this->Indigencies);

        $this->set(compact('indigencies'));
    }

    /**
     * View method
     *
     * @param string|null $id Indigency id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $indigency = $this->Indigencies->get($id, [
            'contain' => [
                'Residents.Puroks',
                'Residents.CivilStatuses'
            ],
        ]);
        $setting = TableRegistry::getTableLocator()->get('Settings')->find()
            ->firstOrFail();

        $this->set(compact('indigency', 'setting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indigency = $this->Indigencies->newEmptyEntity();
        $max = @$this->Indigencies->find()->max('clearance_no')->clearance_no;
        if ($this->request->is('post')) {
            $indigency = $this->Indigencies->patchEntity($indigency, $this->request->getData());
            $indigency->clearance_no = (intval($max) + intval(1));
            if ($this->Indigencies->save($indigency)) {
                $this->Flash->success(__('The indigency has been saved.'));

                return $this->redirect(['action' => 'view', intval($indigency->id)]);
            }
            $this->Flash->error(__('The indigency could not be saved. Please, try again.'));
        }
        $residents = $this->Indigencies->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
            ->all();
        $this->set(compact('indigency', 'residents', 'max'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Indigency id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indigency = $this->Indigencies->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indigency = $this->Indigencies->patchEntity($indigency, $this->request->getData());
            if ($this->Indigencies->save($indigency)) {
                $this->Flash->success(__('The indigency has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The indigency could not be saved. Please, try again.'));
        }
        $residents = $this->Indigencies->Residents->find('list', [
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->order([
            'name' => 'ASC'
        ],true)
            ->all();
        $this->set(compact('indigency', 'residents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Indigency id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indigency = $this->Indigencies->get($id);
        if ($this->Indigencies->delete($indigency)) {
            $this->Flash->success(__('The indigency has been deleted.'));
        } else {
            $this->Flash->error(__('The indigency could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
