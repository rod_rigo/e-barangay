<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * Residents Controller
 *
 * @property \App\Model\Table\ResidentsTable $Residents
 * @method \App\Model\Entity\Resident[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResidentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $search = $this->request->getQuery('search');
        $purokId = $this->request->getQuery('purok_id')? [
            'Residents.purok_id =' => intval($this->request->getQuery('purok_id'))
        ]: null;
        $civilStatusId = $this->request->getQuery('civil_status_id')? [
            'Residents.civil_status_id =' => intval($this->request->getQuery('civil_status_id'))
        ]: null;
        $residentialId = $this->request->getQuery('residential_id')? [
            'Residents.residential_id =' => intval($this->request->getQuery('residential_id'))
        ]: null;
        $genderId = $this->request->getQuery('gender_id')? [
            'Residents.gender_id =' => intval($this->request->getQuery('gender_id'))
        ]: null;
        $query = $this->Residents->find()
            ->where([
               'OR' => [
                   ['Residents.name like' => '%'.($search).'%'],
                   ['Residents.contact_number like' => '%'.($search).'%']
               ]
            ])
            ->where($purokId)
            ->where($civilStatusId)
            ->where($residentialId)
            ->where($genderId)
            ->order([
                'Residents.name' => 'ASC'
            ],true);
        $this->paginate = [
            'contain' => ['Puroks', 'CivilStatuses', 'Genders', 'Residentials'],
        ];
        $residents = $this->paginate($query);

        $puroks = $this->Residents->Puroks->find('list', ['limit' => 200])->all();
        $civilStatuses = $this->Residents->CivilStatuses->find('list', ['limit' => 200])->all();
        $genders = $this->Residents->Genders->find('list', ['limit' => 200])->all();
        $residentials = $this->Residents->Residentials->find('list', ['limit' => 200])->all();
        $this->set(compact('residents', 'puroks', 'civilStatuses', 'genders', 'residentials'));
    }

    /**
     * View method
     *
     * @param string|null $id Resident id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resident = $this->Residents->get($id, [
            'contain' => [
                'Puroks',
                'CivilStatuses',
                'Genders',
                'Residentials',
//                'Issuances'
            ],
        ]);

        $puroks = $this->Residents->Puroks->find('list', ['limit' => 200])->all();
        $civilStatuses = $this->Residents->CivilStatuses->find('list', ['limit' => 200])->all();
        $genders = $this->Residents->Genders->find('list', ['limit' => 200])->all();
        $residentials = $this->Residents->Residentials->find('list', ['limit' => 200])->all();
        $this->set(compact('resident', 'puroks', 'civilStatuses', 'genders', 'residentials'));
    }

    public function barangayClearance($id = null){
        $this->viewBuilder()->setLayout('ajax');
        $resident = $this->Residents->get($id, [
            'contain' => [
                'Puroks',
                'CivilStatuses',
                'Genders',
                'Residentials',
//                'Issuances'
            ],
        ]);
        $this->set(compact('resident'));
    }

    public function barangayIndigency($id = null){
        $this->viewBuilder()->setLayout('ajax');
        $resident = $this->Residents->get($id, [
            'contain' => [
                'Puroks',
                'CivilStatuses',
                'Genders',
                'Residentials',
//                'Issuances'
            ],
        ]);
        $this->set(compact('resident'));
    }

    public function businessClearance($id = null){
        $this->viewBuilder()->setLayout('ajax');
        $resident = $this->Residents->get($id, [
            'contain' => [
                'Puroks',
                'CivilStatuses',
                'Genders',
                'Residentials',
//                'Issuances'
            ],
        ]);
        $this->set(compact('resident'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resident = $this->Residents->newEmptyEntity();
        if ($this->request->is('post')) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());

            if($this->request->getData('file')){

                $file = $this->request->getData('file');

                $filename = uniqid().$file->getCLientFileName();

                $path = WWW_ROOT. 'img'. DS. 'resident-img';
                $folder = new Folder();
                if(!$folder->cd($path)){
                    $folder->create($path);
                }

                $filepath = WWW_ROOT. 'img'. DS. 'resident-img'. DS. $filename;

                if($file){
                    $file->moveTo($filepath);

                }

                $resident->resident_image = 'resident-img/'.($filename);
            }

            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('The resident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The resident could not be saved. Please, try again.'));
        }
        $puroks = $this->Residents->Puroks->find('list', ['limit' => 200])->all();
        $civilStatuses = $this->Residents->CivilStatuses->find('list', ['limit' => 200])->all();
        $genders = $this->Residents->Genders->find('list', ['limit' => 200])->all();
        $residentials = $this->Residents->Residentials->find('list', ['limit' => 200])->all();
        $this->set(compact('resident', 'puroks', 'civilStatuses', 'genders', 'residentials'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Resident id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $resident = $this->Residents->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $resident = $this->Residents->patchEntity($resident, $this->request->getData());

            if($this->request->getData('file')){

                $file = $this->request->getData('file');

                $filename = uniqid().$file->getCLientFileName();

                $path = WWW_ROOT. 'img'. DS. 'resident-img';
                $folder = new Folder();
                if(!$folder->cd($path)){
                    $folder->create($path);
                }

                $path = WWW_ROOT. 'img'. DS. $resident->resident_image;
                $folder = new File($path);
                if($folder->exists()){
                    $folder->delete();
                }

                $filepath = WWW_ROOT. 'img'. DS. 'resident-img'. DS. $filename;

                if($file){
                    $file->moveTo($filepath);

                }

                $resident->resident_image = 'resident-img/'.($filename);
            }

            if ($this->Residents->save($resident)) {
                $this->Flash->success(__('The resident has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The resident could not be saved. Please, try again.'));
        }
        $puroks = $this->Residents->Puroks->find('list', ['limit' => 200])->all();
        $civilStatuses = $this->Residents->CivilStatuses->find('list', ['limit' => 200])->all();
        $genders = $this->Residents->Genders->find('list', ['limit' => 200])->all();
        $residentials = $this->Residents->Residentials->find('list', ['limit' => 200])->all();
        $this->set(compact('resident', 'puroks', 'civilStatuses', 'genders', 'residentials'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Resident id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $resident = $this->Residents->get($id);

        $path = WWW_ROOT. 'img'. DS. $resident->resident_image;
        $folder = new File($path);
        if($folder->exists()){
            $folder->delete();
        }

        if ($this->Residents->delete($resident)) {
            $this->Flash->success(__('The resident has been deleted.'));
        } else {
            $this->Flash->error(__('The resident could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
