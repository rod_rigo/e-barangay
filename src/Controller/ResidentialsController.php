<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Residentials Controller
 *
 * @property \App\Model\Table\ResidentialsTable $Residentials
 * @method \App\Model\Entity\Residential[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResidentialsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $residentials = $this->paginate($this->Residentials);

        $this->set(compact('residentials'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $residential = $this->Residentials->newEmptyEntity();
        if ($this->request->is('post')) {
            $residential = $this->Residentials->patchEntity($residential, $this->request->getData());
            if ($this->Residentials->save($residential)) {
                $this->Flash->success(__('The residential has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The residential could not be saved. Please, try again.'));
        }
        $this->set(compact('residential'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Residential id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $residential = $this->Residentials->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $residential = $this->Residentials->patchEntity($residential, $this->request->getData());
            if ($this->Residentials->save($residential)) {
                $this->Flash->success(__('The residential has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The residential could not be saved. Please, try again.'));
        }
        $this->set(compact('residential'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Residential id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $residential = $this->Residentials->get($id);
        if ($this->Residentials->delete($residential)) {
            $this->Flash->success(__('The residential has been deleted.'));
        } else {
            $this->Flash->error(__('The residential could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
