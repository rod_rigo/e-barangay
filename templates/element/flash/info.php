<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    'use strict';
    $(function (e) {
        Swal.fire({
            icon:'info',
            title: null,
            text:'<?= $message ?>',
            toast:true,
            position:'top-right',
            showConfirmButton:false,
            timerProgressBar:true,
            timer:5000,
        });
    });
</script>
