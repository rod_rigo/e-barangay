<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="javascript:void(0);" class="brand-link d-flex justify-content-center align-items-center">
        <span class="brand-text font-weight-light">
            <strong>E-Barangay</strong>
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
            <img src="<?=$this->Url->assetUrl('/img/baluarte-logo.png')?>" class="img-circle w-100" loading="lazy" title="Deped Logo" alt="DEPED">
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
<!--                <li class="nav-item">-->
<!--                    <a href="=$this->Url->build(['controller' => 'Dashboards', 'action' => 'index'])?>" class="nav-link =ucwords($controller) == ucwords('dashboards')?'active': null?>">-->
<!--                        <i class="nav-icon fas fa-tachometer-alt"></i>-->
<!--                        <p>-->
<!--                            Dashboards-->
<!--                        </p>-->
<!--                    </a>-->
<!--                </li>-->

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Residents', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('residents')?'active': null?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Residents
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Genders', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('genders')?'active': null?>">
                        <i class="nav-icon fas fa-genderless"></i>
                        <p>
                            Genders
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'CivilStatuses', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('CivilStatuses')?'active': null?>">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            Civil-Statuses
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Puroks', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('puroks')?'active': null?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Puroks
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Residentials', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('residentials')?'active': null?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Residentials
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Clearances', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('clearances')?'active': null?>">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Clearances
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Indigencies', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('Indigencies')?'active': null?>">
                        <i class="nav-icon fas fa-file-alt"></i>
                        <p>
                            Indigencies
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'BusinessClearances', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('BusinessClearances')?'active': null?>">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            BusinessClearances
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Settings', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('settings')?'active': null?>">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Setting
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['controller' => 'Users', 'action' => 'index'])?>" class="nav-link <?=ucwords($controller) == ucwords('users')?'active': null?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>


                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" class="nav-link">
                        <i class="nav-icon fas fa-share"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
