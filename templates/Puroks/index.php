<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Purok> $puroks
 */
?>

<div class="card">
    <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
            <a href="<?=$this->Url->build(['controller' => 'Puroks', 'action' => 'add'])?>" type="button" class="btn btn-primary rounded-0">
                New Purok
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="table-responsive">
                    <table id="datatable" class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('purok') ?></th>
                            <th><?= $this->Paginator->sort('created') ?></th>
                            <th><?= $this->Paginator->sort('modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($puroks as $purok): ?>
                            <tr>
                                <td><?= $this->Number->format($purok->id) ?></td>
                                <td><?= h($purok->purok) ?></td>
                                <td><?= h($purok->created) ?></td>
                                <td><?= h($purok->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('Edit'), [
                                        'action' => 'edit', $purok->id
                                    ],[
                                        'class' => 'btn btn-primary'
                                    ]) ?>
                                    <?= $this->Form->postLink(__('Delete'), [
                                        'action' => 'delete', $purok->id
                                    ], [
                                        'confirm' => __('Are you sure you want to delete # {0}?', $purok->id),
                                        'class' => 'btn btn-danger'
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <ul class="pagination pagination-sm m-0 float-right">
            <?= $this->Paginator->first();?>
            <?= $this->Paginator->prev();?>
            <?= $this->Paginator->numbers();?>
            <?= $this->Paginator->next();?>
            <?= $this->Paginator->last();?>
        </ul>
        <?php
        $this->Paginator->setTemplates([
            'first' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>',
            'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
            'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
            'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'ellipsis' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>',
            'last' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>',
            'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
            'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
        ]);
        ?>
    </div>
    <!-- /.card-footer-->
</div>