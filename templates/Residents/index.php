<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Resident> $residents
 */
?>

<div class="card">
    <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
            <a href="<?=$this->Url->build(['controller' => 'Residents', 'action' => 'add'])?>" type="button" class="btn btn-primary rounded-0 float-left pull-left">
                New Resident
            </a>
        </div>
    </div>
    <div class="card-body">
        <?=$this->Form->create(null,['type' => 'get', 'class' => 'row'])?>

            <div class="col-sm-12 col-md-5 col-lg-5 my-2">
                <?=$this->Form->control('search',[
                    'class' => 'form-control',
                    'placeholder' => ucwords('Search Name...'),
                    'type' => 'search'
                ])?>
            </div>
            <div class="col-sm-12 col-md-1 col-lg-1 my-2">
                <?=$this->Form->control('purok_id',[
                    'class' => 'form-control',
                    'options' => $puroks,
                    'empty' => ucwords('Select Purok')
                ])?>
            </div>
            <div class="col-sm-12 col-md-1 col-lg-1 my-2">
                <?=$this->Form->control('gender_id',[
                    'class' => 'form-control',
                    'options' => $genders,
                    'empty' => ucwords('Select Gender')
                ])?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2 my-2">
                <?=$this->Form->control('residential_id',[
                    'class' => 'form-control',
                    'options' => $residentials,
                    'empty' => ucwords('Select Residential')
                ])?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3 my-2">
                <?=$this->Form->control('civil_status_id',[
                    'class' => 'form-control',
                    'options' => $civilStatuses,
                    'empty' => ucwords('Select-Civil Status')
                ])?>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-end align-items-center">
                <?= $this->Form->button(__('Search'),[
                    'class' => 'btn btn-primary rounded-0',
                    'type' => 'submit'
                ]) ?>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                <div class="table-responsive">
                    <table id="datatable" class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('name') ?></th>
                            <th><?= $this->Paginator->sort('resident_image') ?></th>
                            <th><?= $this->Paginator->sort('contact_number') ?></th>
                            <th><?= $this->Paginator->sort('purok_id') ?></th>
                            <th><?= $this->Paginator->sort('civil_status_id') ?></th>
                            <th><?= $this->Paginator->sort('gender_id') ?></th>
                            <th><?= $this->Paginator->sort('birth_date') ?></th>
                            <th><?= $this->Paginator->sort('age') ?></th>
                            <th><?= $this->Paginator->sort('residential_id') ?></th>
                            <th><?= $this->Paginator->sort('created') ?></th>
                            <th><?= $this->Paginator->sort('modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($residents as $resident): ?>
                            <tr>
                                <td><?= $this->Number->format($resident->id) ?></td>
                                <td><?= h($resident->name) ?></td>
                                <td><?= h($resident->resident_image) ?></td>
                                <td><?= $resident->contact_number === null ? '' : $this->Number->format($resident->contact_number) ?></td>
                                <td><?= $resident->has('purok') ? $this->Html->link($resident->purok->purok, ['controller' => 'Puroks', 'action' => 'view', $resident->purok->id]) : '' ?></td>
                                <td><?= $resident->has('civil_status') ? $this->Html->link($resident->civil_status->civil_status, ['controller' => 'CivilStatuses', 'action' => 'view', $resident->civil_status->id]) : '' ?></td>
                                <td><?= $resident->has('gender') ? $this->Html->link($resident->gender->gender, ['controller' => 'Genders', 'action' => 'view', $resident->gender->id]) : '' ?></td>
                                <td><?= h($resident->birth_date) ?></td>
                                <td><?= $this->Number->format($resident->age) ?></td>
                                <td><?= $resident->has('residential') ? $this->Html->link($resident->residential->residential, ['controller' => 'Residentials', 'action' => 'view', $resident->residential->id]) : '' ?></td>
                                <td><?= h($resident->created) ?></td>
                                <td><?= h($resident->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), [
                                        'action' => 'view', $resident->id
                                    ],[
                                        'class' => 'btn btn-primary'
                                    ]) ?>
                                    <?= $this->Form->postLink(__('Delete'), [
                                        'action' => 'delete', $resident->id
                                    ], [
                                        'confirm' => __('Are you sure you want to delete # {0}?', $resident->id),
                                        'class' => 'btn btn-danger'
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?=$this->Form->end()?>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <ul class="pagination pagination-sm m-0 float-right">
            <?= $this->Paginator->first();?>
            <?= $this->Paginator->prev();?>
            <?= $this->Paginator->numbers();?>
            <?= $this->Paginator->next();?>
            <?= $this->Paginator->last();?>
        </ul>
        <?php
        $this->Paginator->setTemplates([
            'first' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>',
            'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
            'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
            'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'ellipsis' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>',
            'last' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>',
            'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
            'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
        ]);
        ?>
    </div>
    <!-- /.card-footer-->
</div>