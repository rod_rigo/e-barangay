<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Resident $resident
 * @var \Cake\Collection\CollectionInterface|string[] $puroks
 * @var \Cake\Collection\CollectionInterface|string[] $civilStatuses
 * @var \Cake\Collection\CollectionInterface|string[] $genders
 * @var \Cake\Collection\CollectionInterface|string[] $residentials
 */
?>

<?= $this->Form->create($resident,['type' => 'file']) ?>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form</h3>
        </div>

        <div class="card-body">
            <div class="row d-flex justify-content-center align-items-center">

                <div class="col-sm-12 col-md-8 col-lg-8 mb-0 d-flex justify-content-center align-items-center">
                    <img src="<?=$this->Url->assetUrl('img/baluarte-logo.png')?>" class="img-thumbnail" loading="lazy" id="image-preview" alt="Logo">
                </div>
                
                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->file('file',[
                        'class' => 'form-control rounded-0',
                        'required' => true,
                        'id' => 'file',
                        'accept' => 'image/*'
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('name',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter name'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('age',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter age'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('birth_date',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter name'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('contact_number',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter contact number'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('gender_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('Select gender'),
                        'options' => $genders
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('purok_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('Select Select Purok'),
                        'options' => $puroks
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('civil_status_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('Select Civil-Status'),
                        'options' => $civilStatuses
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('residential_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('Select Residential'),
                        'options' => $residentials
                    ])?>
                </div>


            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?= $this->Form->button(__('Submit'),[
                'class' => 'btn btn-primary rounded-0'
            ]) ?>
        </div>
        <!-- /.card-footer-->
    </div>
<?= $this->Form->end() ?>

<script>
    'use strict';
    $(document).ready(function (e) {

        $('#file').change(function (e) {

            var file = e.target.files[0];
            var file_reader = new FileReader();

            file_reader.addEventListener('load', function (e) {
                $('#image-preview').attr('src', e.target.result);
            });

            file_reader.readAsDataURL(file);

        });

    });
</script>
