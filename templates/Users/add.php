<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<?= $this->Form->create($user,['type' => 'file']) ?>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form</h3>
        </div>

        <div class="card-body">
            <div class="row d-flex justify-content-center align-items-center">

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('name',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter name'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('username',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter username'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('email',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter email'),
                    ])?>
                </div>

                <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                    <?=$this->Form->control('password',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('Enter password'),
                    ])?>
                </div>

            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?= $this->Form->button(__('Submit'),[
                'class' => 'btn btn-primary rounded-0'
            ]) ?>
        </div>
        <!-- /.card-footer-->
    </div>
<?= $this->Form->end() ?>