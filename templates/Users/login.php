<?php
/**
 *  @var \App\View\AppView $this
 */
?>

<div class="login-box">
    <div class="login-logo">
        <img src="<?=$this->Url->assetUrl('/img/baluarte-logo.png')?>" class="img-circle" height="200" width="200" loading="lazy" alt="Logo" title="Depen Logo">
        <br>
        <a href="javascript:void(0);">
            <b>Welcome To E-Barangay</b>
        </a>
    </div>

    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <?=$this->Form->create(null,['type' => 'file'])?>
            <div class="input-group mb-3">
                <?=$this->Form->text('username',['class' => 'form-control', 'placeholder' => 'Username', 'required'])?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <?=$this->Form->password('password',['class' => 'form-control', 'placeholder' => 'Password', 'required'])?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">

                </div>
                <!-- /.col -->
                <div class="col-4">
                    <?=$this->Form->button('Sign In',['class' => 'btn btn-primary btn-block', 'type' => 'submit'])?>
                </div>
                <!-- /.col -->
            </div>
            <?=$this->Form->end()?>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
