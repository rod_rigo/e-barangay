<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CivilStatus $civilStatus
 */
?>
<?= $this->Form->create($civilStatus,['type' => 'file']) ?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Form</h3>
    </div>

    <div class="card-body">
        <div class="row">

            <div class="col-sm-12 col-md-8 col-lg-8">
                <?=$this->Form->control('civil_status',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter Civil-Status'),
                ])?>
            </div>

        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer d-flex justify-content-end align-items-center">
        <?= $this->Form->button(__('Submit'),[
            'class' => 'btn btn-primary rounded-0'
        ]) ?>
    </div>
    <!-- /.card-footer-->
</div>
<?= $this->Form->end() ?>
