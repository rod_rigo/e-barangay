<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Clearance $clearance
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>e Barangay: Barangay Clearance Report</title>
    <style type="text/css">
        body { font-size: 12pt;
            font-family:Tahoma,sans-serif;
        }
    </style>
    <style type="text/css" media="print">
        @page { size: portrait;
            margin-top:5mm;
            margin-bottom:5mm;
        }
    </style>
</head>
<body>
<table width="800" border="0">
    <thead>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><center><img src="<?=$this->Url->assetUrl('img/baluarte-logo.png')?>" height="133" width="161"></center></td>
        <td><center><strong>Republic of the Philippines<br>
                    City of Santiago<br>
                    BARANGAY BALUARTE<br>
                    -oOo-<br>
                    OFFICE OF THE PUNONG BARANGAY </strong></center></td>
        <td><center><img src="<?=$this->Url->assetUrl('img/santiago-logo.png')?>" height="133" width="161"></center></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><center><strong>BARANGAY CLEARANCE</strong><br>(For Individual)</center></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="250">ALL OFFICERS OF THE LAW<br>and To Whom it May Concern:</td>
        <td>&nbsp;</td>
        <td width="250">Date: <?=date('Y-m-d')?><br>Clearance No. <?=$clearance->clearance_no?></td>
    </tr>
    <tr>
        <td colspan="3"><center><strong>This is to certify that<br><?=strtoupper($clearance->resident->name)?><br>of<br><?=strtoupper($clearance->resident->purok->purok)?> </strong></center></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><left>whose Community Tax Certificate number appears below, is a bonafide resident of this Barangay from <?php date('Y-m-d') ; ?> up to the present. This is to further certify upon verification of the records filed in this office, subject individual was found to have:</left></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><center><strong>NO DEROGATORY RECORD</strong><br>(Records)</center></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><center>This is being used for<br><strong><?php echo ucwords('barangay clearance') ;?></strong><br><?=$clearance->purpose?></center></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><small><center><strong>IMPORTANT REMINDERS:</strong></center></small></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><img src="<?=$this->Url->assetUrl('img/specimensignaturesleftrightthumbmark.png')?>"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><small>
                Community Tax No. <?=$setting->community_tax_number?><br>
                Issued on <?=$clearance->issued_date?><br>
                Issued at <?=$setting->issued_at?><br>
                Amount Paid <?=$clearance->amount_paid?><br>
                Official Receipt No. <?php echo '3062561' ; ?><br>
                Valid up to <?=$clearance->validity?></small>
        </td>
        <td><div id="output"></div></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '';?><br>
            <?php echo '' ;?></td>
        <td>&nbsp;</td>
        <td><?php echo '<br>_____________________<br>'.(strtoupper($setting->captain)).'<br>Punong Barangay' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '';?><br>
            <?php echo '' ;?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><center>NOT VALID WITHOUT BARANGAY SEAL</center></td>
    </tr>


    </tbody>
</table>

<?=$this->Html->script([
    '/plugins/jquery/jquery.min',
    'jquery.qrcode',
    'qrcode'
])?>

<script>
    jQuery(function(){
        jQuery('#output').qrcode("<?php echo "e Barangay: Barangay Clearance Report - Print Date:". (date('Y-m-d')) . " Print Time:" . (date('H:i A')) . " Clearance No." . '17' ; ?>");
    })
</script>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

</body>
</html>

