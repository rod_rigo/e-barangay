<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>

<?= $this->Form->create($setting,['type' => 'file']) ?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Form</h3>
    </div>

    <div class="card-body">
        <div class="row d-flex justify-content-center align-items-center">

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('captain',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter captain'),
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('or_number',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter OR number'),
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('ctc_number',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter CTC number'),
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('community_tax_number',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter community tax number'),
                ])?>
            </div>


            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('issued_at',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter issued at'),
                ])?>
            </div>


        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer d-flex justify-content-end align-items-center">
        <?= $this->Form->button(__('Submit'),[
            'class' => 'btn btn-primary rounded-0'
        ]) ?>
    </div>
    <!-- /.card-footer-->
</div>
<?= $this->Form->end() ?>
