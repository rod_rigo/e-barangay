<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BusinessClearance $businessClearance
 */
?>


<!DOCTYPE html>
<html>
<head>
    <title>e Barangay: Business Clearance Report</title>
    <style type="text/css">
        body { font-size: 12pt;
            font-family:Tahoma,sans-serif;
        }
    </style>
    <style type="text/css" media="print">
        @page { size: portrait;
            margin-top:5mm;
            margin-bottom:5mm;
        }
    </style>
</head>
<body>
<table width="800" border="0">
    <thead>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><center><img src="<?=$this->Url->assetUrl('img/baluarte-logo.png')?>" height="133" width="161"></center></td>
        <td><center><strong>Republic of the Philippines<br>
                    City of Santiago<br>
                    BARANGAY BALUARTE<br>
                    -oOo-<br>
                    OFFICE OF THE PUNONG BARANGAY </strong></center></td>
        <td><center><img src="<?=$this->Url->assetUrl('img/santiago-logo.png')?>" height="133" width="161"></center></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><center><strong>BUSINESS CLEARANCE</strong></center></td>
        <td>&nbsp;</td>
    </tr>
    <!--                <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>   -->
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width="250">Clearance No. <?php echo '17' ; ?><br>Date: <?php echo date('Y-m-d') ;?></td>
    </tr>
    <tr>
        <td colspan="3">OWNER/MANAGER NAME: <?=strtoupper($businessClearance->resident->name)?> <br><br>
            BUSINESS NAME: <?= $businessClearance->business_name;?> <br><br>
            BUSINESS NATURE: <?= $businessClearance->business_nature;?> <br><br>
            BUSINESS ADDRESS: Purok <?=$businessClearance->resident->purok->purok?> Baluarte<br><br>
            PURPOSE: This Business Clearance is being issued upon the request of the above-mentioned person for <?=$businessClearance->business_name;?>. <br>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><small><i>The Issuance of this Clearance is subject to the condition that the above enterprise has complied with and shall continue to comply with all existing national and local government laws, rules & regulations and city & barangay ordinances pertaining to the operation of a business establishment and that its operations shall not be an environmental and social hazard. This Clearance shall be considered automatically revoked, null and void should the above enterprise be found to have violated or failed to comply with the above condition.<br><br><?php echo '' ; ?></i></small></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">Given this <?php echo '' ;?> <?php echo '' ;?><br><?php echo '' ;?><br><?php echo '' ;?><br><?php echo '';?><br><?php echo '' ;?></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><small>Community Tax No. <?=$businessClearance->community_tax_no ; ?><br>
                Issued on <?= $setting->issued_at ; ?><br>
                Issued at <?=$businessClearance->issued_date; ?><br>
                Amount Paid <?= $businessClearance->amount_paid ; ?><br>
                Official Receipt No. <?php echo '' ; ?><br>
                Valid up to <?= $businessClearance->validity ; ?></small>
        </td>
        <td><?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '';?><br>
            <?php echo '' ;?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><center>NOT VALID WITHOUT BARANGAY SEAL</center></td>
    </tr>
    <tr>
        <td><div id="output"></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>


    </tbody>
</table>

<?=$this->Html->script([
    '/plugins/jquery/jquery.min',
    'jquery.qrcode',
    'qrcode'
])?>

<script>
    jQuery(function(){
        jQuery('#output').qrcode("<?php echo "e Barangay: Barangay Clearance Report - Print Date:". (date('Y-m-d')) . " Print Time:" . (date('H:i A')) . " Clearance No." . '17' ; ?>");
    })
</script>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

</body>
</html>

