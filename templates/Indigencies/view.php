<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Indigency $indigency
 */
?>

<!DOCTYPE html>
<html>
<head>
    <title>e Barangay: Indigency Certificate Report</title>
    <style type="text/css">
        body { font-size: 12pt;
            font-family:Tahoma,sans-serif;
        }
    </style>
    <style type="text/css" media="print">
        @page { size: portrait;
            margin-top:5mm;
            margin-bottom:5mm;
        }
    </style>
</head>
<body>
<table width="800" border="0">
    <thead>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><center><img src="<?=$this->Url->assetUrl('img/baluarte-logo.png')?>" height="133" width="161"></center></td>
        <td><center><strong>Republic of the Philippines<br>
                    City of Santiago<br>
                    BARANGAY BALUARTE<br>
                    -oOo-<br>
                    OFFICE OF THE PUNONG BARANGAY </strong></center></td>
        <td><center><img src="<?=$this->Url->assetUrl('img/santiago-logo.png')?>" height="133" width="161"></center></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><center><strong>CERTIFICATE OF INDIGENCY</strong></center></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width="250"><br>Indigency No. <?=$indigency->clearance_no ; ?><br>Date: <?php echo date('Y-m-d') ;?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><strong>TO WHOM IT MAY CONCERN:</strong><br><br> THIS IS TO CERTIFY  that <?php echo strtoupper($indigency->resident->name) ?>, <?php echo $indigency->resident->age ; ?> years of age, Filipino, <?php echo $indigency->resident->civil_status->civil_status ; ?> is a bonafide resident of Purok-<?php echo $indigency->resident->purok->purok ; ?>, <?php echo 'Balurte' ; ?>. He/She is one of those who belong to a low-income family of this barangay.<br><br>THIS CERTIFICATION IS BEING ISSUED upon request of the above named person for whatever lawful purpose(s) it may serve him/her.<br><br>We highly appreciate for any help you can extend to him/her.<br><br>Thank you.
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">ISSUED THIS <?= $indigency->issued_date ;?> <?php echo '' ;?><br><?php echo '' ;?><br><?php echo '' ;?><br><?php echo '';?><br><?php echo '' ;?></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><small><i><?php echo '' ; ?></i></small></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><small>Community Tax No. <?= $setting->community_tax_number ?><br>
                Issued on <?= $indigency->issued_date; ?><br>
                Issued at <?=$setting->issued_at; ?><br>
                Amount Paid <?=$indigency->amount_paid?><br>
                Official Receipt No. <?php echo '0001' ; ?><br>
                Valid up to <?= $indigency->validity; ?></small>
        </td>
        <td><?php echo '<br>_____________________<br>'.(strtoupper($setting->captain)).'<br>Punong Barangay' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '' ;?><br>
            <?php echo '';?><br>
            <?php echo '' ;?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3"><center>NOT VALID WITHOUT BARANGAY SEAL</center></td>
    </tr>
    <tr>
        <td><div id="output"></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>

<?=$this->Html->script([
    '/plugins/jquery/jquery.min',
    'jquery.qrcode',
    'qrcode'
])?>

<script>
    jQuery(function(){
        jQuery('#output').qrcode("<?php echo "e Barangay: Barangay Clearance Report - Print Date:". (date('Y-m-d')) . " Print Time:" . (date('H:i A')) . " Clearance No." . '17' ; ?>");
    })
</script>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

</body>
</html>

