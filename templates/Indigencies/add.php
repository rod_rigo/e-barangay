<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Indigency $indigency
 * @var \Cake\Collection\CollectionInterface|string[] $residents
 */
?>

<?= $this->Form->create($indigency,['type' => 'file']) ?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Form</h3>
    </div>

    <div class="card-body">
        <div class="row d-flex justify-content-center align-items-center">

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('resident_id', [
                    'options' => $residents,
                    'class' => 'form-control rounded-0',
                    'empty' => ucwords('select resident')
                ])?>
            </div>


            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('clearance_no',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter clearance no'),
                    'value' => intval($max)
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('purpose',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter purpose'),
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('issued_date',[
                    'class' => 'form-control rounded-0',
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('amount_paid',[
                    'class' => 'form-control rounded-0',
                    'placeholder' => ucwords('Enter amount paid'),
                ])?>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                <?=$this->Form->control('validity',[
                    'class' => 'form-control rounded-0',
                ])?>
            </div>

        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer d-flex justify-content-end align-items-center">
        <?= $this->Form->button(__('Submit'),[
            'class' => 'btn btn-primary rounded-0'
        ]) ?>
    </div>
    <!-- /.card-footer-->
</div>
<?= $this->Form->end() ?>

