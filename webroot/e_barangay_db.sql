/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : e_barangay_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 03/06/2024 10:47:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for business_clearances
-- ----------------------------
DROP TABLE IF EXISTS `business_clearances`;
CREATE TABLE `business_clearances`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `resident_id` int UNSIGNED NOT NULL,
  `business_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `business_nature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `community_tax_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `issued_date` datetime NOT NULL,
  `amount_paid` double NOT NULL,
  `or_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `validity` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of business_clearances
-- ----------------------------
INSERT INTO `business_clearances` VALUES (1, 2, 'asdas', 'asdasdad', 'asda', '2024-06-03 10:34:11', 1200, 'dasdas', '2024-06-30', '2024-06-03 10:35:16', '2024-06-03 10:35:16');

-- ----------------------------
-- Table structure for civil_statuses
-- ----------------------------
DROP TABLE IF EXISTS `civil_statuses`;
CREATE TABLE `civil_statuses`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `civil_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of civil_statuses
-- ----------------------------
INSERT INTO `civil_statuses` VALUES (1, 'single', '2024-05-28 10:14:47', '2024-05-28 10:14:47');

-- ----------------------------
-- Table structure for clearances
-- ----------------------------
DROP TABLE IF EXISTS `clearances`;
CREATE TABLE `clearances`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `resident_id` int UNSIGNED NOT NULL,
  `clearance_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `issued_date` date NOT NULL,
  `amount_paid` double NOT NULL,
  `validity` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of clearances
-- ----------------------------
INSERT INTO `clearances` VALUES (1, 2, '1', 'clearnces', '2024-06-03', 120, '2024-06-30', '2024-06-03 09:49:29', '2024-06-03 09:49:29');
INSERT INTO `clearances` VALUES (2, 1, '2', 'asdsada', '2024-06-30', 120, '2024-06-30', '2024-06-03 09:59:07', '2024-06-03 09:59:07');

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of genders
-- ----------------------------
INSERT INTO `genders` VALUES (1, 'Male', '2024-05-28 09:56:11', '2024-05-28 10:05:48');

-- ----------------------------
-- Table structure for indigencies
-- ----------------------------
DROP TABLE IF EXISTS `indigencies`;
CREATE TABLE `indigencies`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `resident_id` int UNSIGNED NOT NULL,
  `clearance_no` int NOT NULL,
  `purpose` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `issued_date` date NOT NULL,
  `amount_paid` double NOT NULL,
  `validity` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of indigencies
-- ----------------------------
INSERT INTO `indigencies` VALUES (1, 2, 0, 'asdasdasd', '2024-06-12', 120, '2024-06-13', '2024-06-03 10:06:42', '2024-06-03 10:06:42');

-- ----------------------------
-- Table structure for issuances
-- ----------------------------
DROP TABLE IF EXISTS `issuances`;
CREATE TABLE `issuances`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `resident_id` int NOT NULL,
  `issuance` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `year` year NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of issuances
-- ----------------------------

-- ----------------------------
-- Table structure for puroks
-- ----------------------------
DROP TABLE IF EXISTS `puroks`;
CREATE TABLE `puroks`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `purok` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of puroks
-- ----------------------------
INSERT INTO `puroks` VALUES (1, '2', '2024-05-28 10:21:36', '2024-05-28 10:22:50');

-- ----------------------------
-- Table structure for residentials
-- ----------------------------
DROP TABLE IF EXISTS `residentials`;
CREATE TABLE `residentials`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `residential` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of residentials
-- ----------------------------
INSERT INTO `residentials` VALUES (1, 'transfered', '2024-05-28 10:26:33', '2024-05-28 10:26:38');

-- ----------------------------
-- Table structure for residents
-- ----------------------------
DROP TABLE IF EXISTS `residents`;
CREATE TABLE `residents`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resident_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `purok_id` int NOT NULL,
  `civil_status_id` int NOT NULL,
  `gender_id` int NOT NULL,
  `birth_date` date NOT NULL,
  `age` int NOT NULL,
  `residential_id` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of residents
-- ----------------------------
INSERT INTO `residents` VALUES (1, 'Marc Jeff', 'resident-img/66555291575edBrain-network-japan-logo-1080x1080.png', '09100575757', 1, 1, 1, '2024-05-15', 20, 1, '2024-05-28 11:37:40', '2024-05-28 11:42:09');
INSERT INTO `residents` VALUES (2, 'applicant applicant applicant', 'resident-img/665552b01fc7d7a764272-04ad-43bd-98dc-af76f7161bba.jpg', '09100565788', 1, 1, 1, '2024-05-28', 22, 1, '2024-05-28 11:38:33', '2024-05-28 11:42:40');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `captain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `or_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ctc_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `community_tax_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `issued_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (1, 'asdas', 'dasdas', 'asda', 'asda', 'asdasd', '2024-06-02 22:58:51', '2024-06-02 22:58:51');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$cq335g1SnU7kPVRpn3m3MePsRyQ8lFeGzkCQ9fV1u5nzqexYmxQUi', '2024-05-28 10:38:52', '2024-06-03 09:27:34');

SET FOREIGN_KEY_CHECKS = 1;
